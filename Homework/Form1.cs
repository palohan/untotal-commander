﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DLL;

namespace Homework
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            dirPan1.OtherPanel = dirPan2;
             
            dirPan1.ShowFolder(Properties.Settings.Default.LastDir1);            
            dirPan2.ShowFolder(Properties.Settings.Default.LastDir2);
        }

        enum FileOp
        {
            copy, move
        }

        bool nothingSelected                                  //neni nic vyznacene
        {
            get { return dirPan1.FileDisplay.SelectedItems.Count + dirPan2.FileDisplay.SelectedItems.Count == 0; }
        }

        bool SomeDirsEmpty
        {
            get { return dirPan1.CurrentDirectory == null || dirPan2.CurrentDirectory == null; }
        }

        bool AllDirsEmpty
        {
            get { return dirPan1.CurrentDirectory == null && dirPan2.CurrentDirectory == null; }
        }

       
        private void Copy_Click(object sender, EventArgs e)
        {
            TwoPanelOperation(FileOp.copy);
        }

        private void Move_Click(object sender, EventArgs e)
        {
            TwoPanelOperation(FileOp.move);
        }

        void TwoPanelOperation(FileOp op)
        {
            if (SomeDirsEmpty)         //nema nacitane v okne
                return;

            bool ask = true;                                    //cielovy folder to uz ma

            if (nothingSelected)                         //neni nic nikde oznacene
                return;

            try
            {
                foreach (var item in DirPanel.FocusedPanel.FileDisplay.SelectedItems.OfType<FilePanelItem>())
                {
                    if (item.Text == "..")                    //ignorujeme
                        continue;

                    if (ask && DirPanel.FocusedPanel.OtherPanel.ContainsItem(item)) //ma sa pytat
                    {
                        var dialogResult = new AskDialog($"{(item.IsDir ? "Folder" : "File")} {item.FileSysInfo.Name} already exists. Overwrite?").ShowDialog();

                        switch (dialogResult)
                        {
                            case DialogResult.OK:           //YesTOall
                                ask = false;                //nepytaj sa a pokracuj
                                break;

                            case DialogResult.Ignore:       //no to all
                                ask = false;                //nepytaj sa
                                continue;                   //kasli na tento folder a id na dalsi

                            case DialogResult.No:           //pytaj sa a id na dalsi
                                continue;

                            case DialogResult.Abort:        //zlakol sa
                                return;                     //ser na to setko

                            case DialogResult.Yes:          //yes na 1 folder netreba, pojde dalej
                                break;
                        }
                    }

                    string sourcePath = item.FileSysInfo.FullName;
                    string destPath = Path.Combine(DirPanel.FocusedPanel.OtherPanel.CurrentDirectory.FullName, item.FileSysInfo.Name);

                    switch (op)
                    {
                        case FileOp.copy:

                            if (item.IsDir)
                                Extensions.CopyTree(sourcePath, destPath);
                            else
                                File.Copy(sourcePath, destPath, true);
                            break;

                        case FileOp.move:
                            if (item.IsDir)
                            {
                                if (Directory.Exists(destPath))             //na tej ceste uz existuje
                                    Directory.Delete(destPath);

                                Directory.Move(sourcePath, destPath);
                            }
                            else
                            {
                                if (File.Exists(destPath))
                                    File.Delete(destPath);

                                File.Move(sourcePath, destPath);
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            DirPanel.FocusedPanel.OtherPanel.FillDisplay();
        }


        private void Create_Click(object sender, EventArgs e)
        {
            if (AllDirsEmpty)         //nema nacitane v okne
                return;

            NewFolderDialog win = new NewFolderDialog("New folder");
            
            if (win.ShowDialog() == DialogResult.OK)
            {
                string newDirPath = Path.Combine(dirPan1.CurrentDirectory.FullName, win.ItemText);
                try
                {
                    if (Directory.Exists(newDirPath))
                        if (DialogResult.No == MessageBox.Show("Directory already exists.Overwrite?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                            return;
                        else
                            Directory.Delete(newDirPath, true);

                    Directory.CreateDirectory(newDirPath);
                    dirPan1.FillDisplay();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }


        private void Delete_Click(object sender, EventArgs e)           //vymazanie
        {
            if (nothingSelected)
                return;

            bool ask = true;

            foreach (var item in DirPanel.FocusedPanel.FileDisplay.SelectedItems.Cast<FilePanelItem>())    //pre vsetky oznacene
            {
                if (item.Text == "..")                    //ignorujeme
                    continue;

                string itemPath = item.FileSysInfo.FullName;

                if (item.IsDir)                              //je DIR
                {
                    if (item.FileSysInfo.Exists)                                 //pre istotu
                    {
                        DirectoryInfo dir = new DirectoryInfo(itemPath);
                        
                        if (ask && dir.GetDirectories().Any())    //ma subdiry a ma sa pytat
                        {
                            var diRes = new AskDialog(string.Format("Delete subdirectories from {0}?", dir.Name)).ShowDialog();
                            
                            switch (diRes)
                            {
                                case DialogResult.OK:       //YesTOall
                                    ask = false;            //nepytaj sa a pokracuj
                                    break;
                                
                                case DialogResult.Ignore:   //no to all
                                    ask = false;            //nepytaj sa
                                    continue;               //kasli na tento folder a id na dalsi
                                
                                case DialogResult.No:       //pytaj sa a id na dalsi
                                    continue;

                                case DialogResult.Abort:
                                    return;                 //ser na to setko
                                //yes netreba, pojde dalej
                            }
                        }
                        try
                        {
                            Directory.Delete(itemPath, true);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                }
                else                                                    //je to file
                {
                    try
                    {
                        File.Delete(itemPath);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(String.Format("Couldn't delete {0}.", itemPath));
                    }
                }
            }

            DirPanel.FocusedPanel.FillDisplay();        //updatuj display
        }


        private void Rename_Click(object sender, EventArgs e)
        {
            if (SomeDirsEmpty || nothingSelected)         //nema nacitane v okne a neni nic oznacene
                return;


            string currentDir = DirPanel.FocusedPanel.CurrentDirectory.FullName;
            var selItem = DirPanel.FocusedPanel.FileDisplay.SelectedItems[0] as FilePanelItem;      //len prva vec
                
            if (selItem.Text == "..")                      //ignorujeme
                return;

            string sourceName = selItem.FileSysInfo.Name;
            string sourcePath = selItem.FileSysInfo.FullName;
            

            NewFolderDialog renameDialog = new NewFolderDialog("New name:", sourceName);
            if (renameDialog.ShowDialog() == DialogResult.OK)
            {
                string newPath = Path.Combine(currentDir, renameDialog.ItemText);

                if (newPath == sourcePath)
                    return;

                if (selItem.IsDir)                     //je DIR
                {
                    if (Directory.Exists(newPath))
                    {
                        if (MessageBox.Show("Directory already exists. Overwrite?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                            return; //nechce prepisat, koncime
                        else
                            Directory.Delete(newPath);
                    }
                    Directory.Move(sourcePath, newPath);

                }
                else                                    //je file
                {
                    if (File.Exists(newPath))
                    {
                        if (DialogResult.No == MessageBox.Show("File already exists. Overwrite?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                            return;                          //nechce prepisat, koncime
                        else
                            File.Delete(newPath);
                    }
                    File.Move(sourcePath, newPath);

                }

                DirPanel.FocusedPanel.FillDisplay();
            }
        }

        private void MainForm_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyData)
            {
                case Keys.Delete:
                    Delete_Click(sender, e);
                    break;
            }
        }

        private void swapLR_Click(object sender, EventArgs e)
        {
            swapPanel(dirPan1);
        }

        private void swapRL_Click(object sender, EventArgs e)
        {
            swapPanel(dirPan2);
        }

        private void swapPanel(DirPanel fromPanel)
        {
            fromPanel.OtherPanel.cancelDriveEvent = true;
            fromPanel.OtherPanel.ShowFolder(fromPanel.CurrentDirectory);
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Properties.Settings.Default.LastDir1 = dirPan1.CurrentDirectory.FullName;
            Properties.Settings.Default.LastDir2 = dirPan2.CurrentDirectory.FullName;

            Properties.Settings.Default.Save();
        }
    }
}
