﻿using System.ComponentModel;
using System.Windows.Forms;
using DLL;

namespace Homework
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Copy = new System.Windows.Forms.Button();
            this.Create = new System.Windows.Forms.Button();
            this.Delete = new System.Windows.Forms.Button();
            this.Rename = new System.Windows.Forms.Button();
            this.BtMove = new System.Windows.Forms.Button();
            this.swapLR = new System.Windows.Forms.Button();
            this.swapRL = new System.Windows.Forms.Button();
            this.dirPan2 = new DLL.DirPanel();
            this.dirPan1 = new DLL.DirPanel();
            this.SuspendLayout();
            // 
            // Copy
            // 
            this.Copy.Location = new System.Drawing.Point(97, 299);
            this.Copy.Name = "Copy";
            this.Copy.Size = new System.Drawing.Size(75, 23);
            this.Copy.TabIndex = 4;
            this.Copy.Text = "Copy";
            this.Copy.UseVisualStyleBackColor = true;
            this.Copy.Click += new System.EventHandler(this.Copy_Click);
            // 
            // Create
            // 
            this.Create.Location = new System.Drawing.Point(178, 299);
            this.Create.Name = "Create";
            this.Create.Size = new System.Drawing.Size(75, 23);
            this.Create.TabIndex = 7;
            this.Create.Text = "Create";
            this.Create.UseVisualStyleBackColor = true;
            this.Create.Click += new System.EventHandler(this.Create_Click);
            // 
            // Delete
            // 
            this.Delete.Location = new System.Drawing.Point(259, 299);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(75, 23);
            this.Delete.TabIndex = 8;
            this.Delete.Text = "Delete";
            this.Delete.UseVisualStyleBackColor = true;
            this.Delete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // Rename
            // 
            this.Rename.Location = new System.Drawing.Point(340, 299);
            this.Rename.Name = "Rename";
            this.Rename.Size = new System.Drawing.Size(75, 23);
            this.Rename.TabIndex = 9;
            this.Rename.Text = "Rename";
            this.Rename.UseVisualStyleBackColor = true;
            this.Rename.Click += new System.EventHandler(this.Rename_Click);
            // 
            // MOve
            // 
            this.BtMove.Location = new System.Drawing.Point(421, 299);
            this.BtMove.Name = "MOve";
            this.BtMove.Size = new System.Drawing.Size(75, 23);
            this.BtMove.TabIndex = 10;
            this.BtMove.Text = "Move";
            this.BtMove.UseVisualStyleBackColor = true;
            this.BtMove.Click += new System.EventHandler(this.Move_Click);
            // 
            // swapLR
            // 
            this.swapLR.Location = new System.Drawing.Point(290, 133);
            this.swapLR.Name = "swapLR";
            this.swapLR.Size = new System.Drawing.Size(44, 23);
            this.swapLR.TabIndex = 11;
            this.swapLR.Text = "=>";
            this.swapLR.UseVisualStyleBackColor = true;
            this.swapLR.Click += new System.EventHandler(this.swapLR_Click);
            // 
            // swapRL
            // 
            this.swapRL.Location = new System.Drawing.Point(290, 162);
            this.swapRL.Name = "swapRL";
            this.swapRL.Size = new System.Drawing.Size(44, 23);
            this.swapRL.TabIndex = 12;
            this.swapRL.Text = "<=";
            this.swapRL.UseVisualStyleBackColor = true;
            this.swapRL.Click += new System.EventHandler(this.swapRL_Click);
            // 
            // dirPan2
            //             
            this.dirPan2.Location = new System.Drawing.Point(340, 12);
            this.dirPan2.Name = "dirPan2";
            this.dirPan2.Size = new System.Drawing.Size(272, 270);
            this.dirPan2.TabIndex = 6;
            // 
            // dirPan1
            //             
            this.dirPan1.Location = new System.Drawing.Point(12, 12);
            this.dirPan1.Name = "dirPan1";
            this.dirPan1.Size = new System.Drawing.Size(272, 270);
            this.dirPan1.TabIndex = 5;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 334);
            this.Controls.Add(this.swapRL);
            this.Controls.Add(this.swapLR);
            this.Controls.Add(this.BtMove);
            this.Controls.Add(this.Rename);
            this.Controls.Add(this.Delete);
            this.Controls.Add(this.Create);
            this.Controls.Add(this.dirPan2);
            this.Controls.Add(this.dirPan1);
            this.Controls.Add(this.Copy);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "UnTotalCommander";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyUp);
            this.ResumeLayout(false);

        }

        #endregion

        private Button Copy;
        private DirPanel dirPan1;
        private DirPanel dirPan2;
        private Button Create;
        private Button Delete;
        private Button Rename;
        private Button BtMove;
        private Button swapLR;
        private Button swapRL;
    }
}

