﻿namespace DLL
{
    partial class NewFolderDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.caption = new System.Windows.Forms.Label();
            this.NewFolderBox = new System.Windows.Forms.ComboBox();
            this.OKbut = new System.Windows.Forms.Button();
            this.CancelBut = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.caption.AutoSize = true;
            this.caption.Location = new System.Drawing.Point(12, 9);
            this.caption.Name = "label1";
            this.caption.Size = new System.Drawing.Size(61, 13);
            this.caption.TabIndex = 0;
            this.caption.Text = "New folder:";
            // 
            // NewFolder
            // 
            this.NewFolderBox.FormattingEnabled = true;
            this.NewFolderBox.Location = new System.Drawing.Point(12, 25);
            this.NewFolderBox.Name = "NewFolder";
            this.NewFolderBox.Size = new System.Drawing.Size(251, 21);
            this.NewFolderBox.TabIndex = 1;
            // 
            // OKbut
            // 
            this.OKbut.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OKbut.Location = new System.Drawing.Point(107, 52);
            this.OKbut.Name = "OKbut";
            this.OKbut.Size = new System.Drawing.Size(75, 23);
            this.OKbut.TabIndex = 2;
            this.OKbut.Text = "OK";
            this.OKbut.UseVisualStyleBackColor = true;
            // 
            // CancelBut
            // 
            this.CancelBut.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelBut.Location = new System.Drawing.Point(188, 52);
            this.CancelBut.Name = "CancelBut";
            this.CancelBut.Size = new System.Drawing.Size(75, 23);
            this.CancelBut.TabIndex = 3;
            this.CancelBut.Text = "Cancel";
            this.CancelBut.UseVisualStyleBackColor = true;
            // 
            // New
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(272, 95);
            this.Controls.Add(this.CancelBut);
            this.Controls.Add(this.OKbut);
            this.Controls.Add(this.NewFolderBox);
            this.Controls.Add(this.caption);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "New";
            this.Text = "New";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label caption;
        private System.Windows.Forms.ComboBox NewFolderBox;
        private System.Windows.Forms.Button OKbut;
        private System.Windows.Forms.Button CancelBut;
    }
}