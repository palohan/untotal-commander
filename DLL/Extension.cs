﻿using System;
using System.IO;
using System.Windows.Forms;

namespace DLL
{
    public static class Extensions
    {
        public static void CopyTree(string source, string dest)
        {
            if (!Directory.Exists(source))
                return;

            DirectoryInfo DIR = new DirectoryInfo(source);

            try
            {
                if (Directory.Exists(dest))
                    Directory.Delete(dest, true);

                Directory.CreateDirectory(dest);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            foreach (var f in DIR.GetFiles())
                f.CopyTo(Path.Combine(dest, f.Name), true);

            foreach (var d in DIR.GetDirectories())
                CopyTree(d.FullName, Path.Combine(dest, d.Name));
        }

        public static bool ExtCompare(string left, string right)
        {
            return string.Compare(right, left, true) == 0;
        }

        public static bool IsDir(this FileSystemInfo info)
        {
            return info.Attributes.HasFlag(FileAttributes.Directory);
        }

        public static bool HasExtension(this FileSystemInfo info, string extension)
        {            
            return ExtCompare(info.Extension, extension);
        }

        public static bool IsTextFile(this FileSystemInfo info)
        {
            return HasExtension(info, ".txt");
        }
    }
}
