﻿using System.Windows.Forms;

namespace DLL
{
    public partial class NewFolderDialog : Form
    {
        public NewFolderDialog(string caption, string itemName = "")
        {
            InitializeComponent();
            this.caption.Text = caption;

            NewFolderBox.Text = itemName;
        }

        public string ItemText { get { return NewFolderBox.Text; } }
    }
}
