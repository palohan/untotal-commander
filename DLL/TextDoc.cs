﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace DLL
{
    public partial class TextDoc : Form
    {
        static Dictionary<string, TextDoc> OpenedTexts = new Dictionary<string, TextDoc>();

        private FileInfo textFileInfo;

        static public void OpenEditor(FileInfo fInfo)
        {
            TextDoc editor;
            OpenedTexts.TryGetValue(fInfo.FullName, out editor);

            if (editor != null)
                editor.BringToFront();
            else
            {
                new TextDoc(fInfo).Show();
            }
        }

        private TextDoc(FileInfo fInfo)
        {
            InitializeComponent();

            textFileInfo = fInfo;
            Text = textFileInfo.FullName;

            ReadTextFile(fInfo);

            OpenedTexts.Add(fInfo.FullName, this);
        }

        void ReadTextFile(FileInfo fInfo)
        {
            TextReader reader = textFileInfo.OpenText();

            string line;
            while ((line = reader.ReadLine()) != null)
            {
                textDisplay.Text += line + Environment.NewLine;
            }
            reader.Close();

            textDisplay.SelectionStart = 0;
        }

        void SaveTextFile(FileInfo fInfo)
        {
            TextWriter writer = File.CreateText(fInfo.FullName);

            foreach (string line in textDisplay.Lines)
                writer.WriteLine(line);

            writer.Close();
        }

        private void TextDoc_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (textDisplay.Modified)
            {
                DialogResult result = MessageBox.Show(string.Format("Save changes to {0}?", textFileInfo.FullName), "File changed", MessageBoxButtons.YesNoCancel,
                                                   MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                    SaveTextFile(textFileInfo);
                else if (result == DialogResult.Cancel)
                    e.Cancel = true;
            }
        }

        private void TextDoc_FormClosed(object sender, FormClosedEventArgs e)
        {
            OpenedTexts.Remove(textFileInfo.FullName);
        }
    }
}
