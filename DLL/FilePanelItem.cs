﻿namespace DLL
{
    using System.IO;
    using System.Windows.Forms;

    public class FilePanelItem : ListViewItem
    {
        private DirPanel parentPanel; 
                
        public FileSystemInfo FileSysInfo { get; }

        public bool IsDir { get; private set; }


        public FilePanelItem(FileSystemInfo fileInfo, DirPanel parent)
        {
            FileSysInfo = fileInfo;

            IsDir = fileInfo.IsDir();
            
            Text = IsDir ? fileInfo.Name : Path.GetFileNameWithoutExtension(fileInfo.Name);

            SubItems.Add(IsDir ? "<DIR>" : fileInfo.Extension);

            parentPanel = parent;
        }

        public override string ToString()
        {
            return FileSysInfo.Name;
        }
    }
}
