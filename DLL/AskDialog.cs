﻿using System.Windows.Forms;

namespace DLL
{
    public partial class AskDialog : Form
    {
        public AskDialog(string text)
        {
            InitializeComponent();
            label1.Text = text;
        }
    }
}
