﻿namespace DLL
{
    partial class DirPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fileDisplay = new System.Windows.Forms.ListView();
            this.Meno = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Ext = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.FileSelector = new System.Windows.Forms.ComboBox();
            this.Drive = new System.Windows.Forms.ComboBox();
            this.showHidden = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // fileDisplay
            // 
            this.fileDisplay.BackColor = System.Drawing.SystemColors.Window;
            this.fileDisplay.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Meno,
            this.Ext});
            this.fileDisplay.Location = new System.Drawing.Point(3, 32);
            this.fileDisplay.Name = "fileDisplay";
            this.fileDisplay.Size = new System.Drawing.Size(260, 211);
            this.fileDisplay.TabIndex = 2;
            this.fileDisplay.UseCompatibleStateImageBehavior = false;
            this.fileDisplay.View = System.Windows.Forms.View.Details;
            this.fileDisplay.DoubleClick += new System.EventHandler(this.FileDisplay_SelectedIndexChanged_1);
            this.fileDisplay.Enter += new System.EventHandler(this.FileDisplay_Enter);
            this.fileDisplay.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FileDisplay_KeyUp);
            // 
            // Meno
            // 
            this.Meno.Text = "Name";
            this.Meno.Width = 111;
            // 
            // Ext
            // 
            this.Ext.Text = "Ext";
            // 
            // FileSelector
            // 
            this.FileSelector.FormattingEnabled = true;
            this.FileSelector.Location = new System.Drawing.Point(58, 5);
            this.FileSelector.Name = "FileSelector";
            this.FileSelector.Size = new System.Drawing.Size(205, 21);
            this.FileSelector.TabIndex = 4;
            this.FileSelector.SelectedIndexChanged += new System.EventHandler(this.FileSelector_SelectedIndexChanged);
            this.FileSelector.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.Supress_MouseWheel);
            this.FileSelector.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Supress_Key);
            //this.FileSelector.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FileSelector_KeyUp);
            //this.FileSelector.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Supress_Key);
            // 
            // Drive
            // 
            this.Drive.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Drive.FormattingEnabled = true;
            this.Drive.Location = new System.Drawing.Point(3, 5);
            this.Drive.Name = "Drive";
            this.Drive.Size = new System.Drawing.Size(49, 21);
            this.Drive.TabIndex = 5;
            this.Drive.SelectedIndexChanged += new System.EventHandler(this.Drive_SelectedIndexChanged);
            this.Drive.Click += new System.EventHandler(this.Drive_Click);
            this.Drive.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.Supress_MouseWheel);
            // 
            // showHidden
            // 
            this.showHidden.AutoSize = true;
            this.showHidden.Location = new System.Drawing.Point(3, 249);
            this.showHidden.Name = "showHidden";
            this.showHidden.Size = new System.Drawing.Size(86, 17);
            this.showHidden.TabIndex = 6;
            this.showHidden.Text = "show hidden";
            this.showHidden.UseVisualStyleBackColor = true;
            this.showHidden.CheckedChanged += new System.EventHandler(this.ShowHidden_CheckedChanged);
            // 
            // DirPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.showHidden);
            this.Controls.Add(this.Drive);
            this.Controls.Add(this.FileSelector);
            this.Controls.Add(this.fileDisplay);
            this.Name = "DirPanel";
            this.Size = new System.Drawing.Size(272, 271);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView fileDisplay;
        private System.Windows.Forms.ColumnHeader Meno;
        private System.Windows.Forms.ColumnHeader Ext;
        private System.Windows.Forms.ComboBox FileSelector;
        private System.Windows.Forms.ComboBox Drive;
        private System.Windows.Forms.CheckBox showHidden;

    }
}
