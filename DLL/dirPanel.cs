﻿namespace DLL
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Windows.Forms;

    public partial class DirPanel : UserControl
    {
        public DirectoryInfo CurrentDirectory { get; private set; }

        public bool cancelDriveEvent = false;

        private DirPanel otherPanel = null;

        public DirPanel OtherPanel
        {
            get
            {
                return otherPanel;
            }
            set
            {
                otherPanel = value;
                value.otherPanel = this;
            }
        }

        public ListView FileDisplay         //pristup k fileDisplay z designera
        {
            get { return fileDisplay; }
        }

        public static DirPanel FocusedPanel { get; private set; }


        public DirPanel()
        {
            InitializeComponent();


            DriveInfo drInfo;
            bool firstDriveSet = false;

            foreach (var drive in Environment.GetLogicalDrives())
            {
                drInfo = new DriveInfo(drive);

                if (!drInfo.IsReady)
                    continue;

                Drive.Items.Add(drive);                             //pridame drivy

                if (!firstDriveSet && drInfo.DriveType == DriveType.Fixed)
                {
                    //Drive.SelectedValue = Drive.Items[Drive.Items.Count - 1];
                    Drive.SelectedIndex = Drive.FindStringExact(drive);                 //toto ukaze 1. fixed drive
                    firstDriveSet = true;
                }
            }
        }


        public bool ContainsItem(FilePanelItem other)
        {
            var has = CurrentDirectory.GetFileSystemInfos(other.FileSysInfo.Name, SearchOption.TopDirectoryOnly).Length > 0;

            return has;
        }

        public void ShowFolder(string folder)                   //MAX DOLEZITA METODA
        {
            try
            {
                ShowFolder(new DirectoryInfo(folder));
            }
            catch (ArgumentException)                           //supress bad path
            {
                ;               
            }
        }

        public void ShowFolder(DirectoryInfo dirInfo)
        {
            try
            {
                if (!dirInfo.Exists)
                    return;

                CurrentDirectory = dirInfo;

                Drive.SelectedItem = CurrentDirectory.Root.Name;

                FillDisplay();
                FileSelector.Text = dirInfo.FullName;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }


        public void FillDisplay(bool updateOther = true)
        {
            if (CurrentDirectory == null)
                return;

            fileDisplay.Items.Clear();                      //vypraznime
            FileSelector.Items.Clear();

            if (CurrentDirectory.FullName != CurrentDirectory.Root.FullName)    //ak nie sme v koreni stromu, pridame ..
            {
                fileDisplay.Items.Add("..");
            }

            foreach (DirectoryInfo dirInfo in CurrentDirectory.GetDirectories())
            {
                if ((dirInfo.Attributes.HasFlag(FileAttributes.System)))        //je systemovy, ten ne
                    continue;

                if (!showHidden.Checked && (dirInfo.Attributes.HasFlag(FileAttributes.Hidden)))        //skryte nie, ak nechce
                    continue;

                var fpItem = new FilePanelItem(dirInfo, this);

                fileDisplay.Items.Add(fpItem);
                FileSelector.Items.Add(fpItem);
            }

            foreach (FileInfo fileInfo in CurrentDirectory.GetFiles())
                fileDisplay.Items.Add(new FilePanelItem(fileInfo, this));

            if (updateOther && otherPanel != null && otherPanel.CurrentDirectory != null && otherPanel.CurrentDirectory.FullName == CurrentDirectory.FullName)    //ak brat ukazuje rovnaky dir, updatni ho
                otherPanel.FillDisplay(false);                                    //aby sa rekurzivne neupdatovali
        }


        private void FileDisplay_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            try
            {
                if (fileDisplay.SelectedItems[0].Text == "..")
                {
                    ShowFolder(CurrentDirectory.Parent);
                    return;
                }

                var selItem = fileDisplay.SelectedItems[0] as FilePanelItem;

                OpenItem(selItem.FileSysInfo);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void OpenItem(FileSystemInfo fInfo)             //otvori vec
        {
            try
            {
                if (fInfo.IsDir())                     //je to dir
                    ShowFolder(fInfo as DirectoryInfo);                  //IBA HO UKAZ
                else if (fInfo.IsTextFile())                      //je to text file
                    TextDoc.OpenEditor(fInfo as FileInfo);
                else                                            //neni to dir ani .txt
                    Process.Start(fInfo.FullName);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                
                if (CurrentDirectory.Parent != null)
                    OpenItem(CurrentDirectory.Parent);
            }
        }

        private void FileSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CurrentDirectory != null)
            {
                var dirInfo = (FileSelector.SelectedItem as FilePanelItem).FileSysInfo;
                OpenItem(dirInfo);

                BeginInvoke((Action)(() =>
                {
                    FileSelector.Text = dirInfo.FullName;
                }));
            }            
        }

        private void Drive_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cancelDriveEvent)
            {
                cancelDriveEvent = false;
                return;
            }

            ShowFolder(Drive.SelectedItem.ToString());
        }

        private void Drive_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (var s in Environment.GetLogicalDrives())
                {
                    var driveInfo = new DriveInfo(s);

                    if (!driveInfo.IsReady && Drive.Items.Contains(s))          //drive neni ready a list ju obsahuje
                        Drive.Items.Remove(s);                                  //vymaz ju

                    if (driveInfo.IsReady && !Drive.Items.Contains(s))          //drive je ready a list ju neobsahuje
                        Drive.Items.Add(s);                                     //pridaj ju
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        void Supress_MouseWheel(object sender, MouseEventArgs e)
        {
            ((HandledMouseEventArgs)e).Handled = true;
        }

        void Supress_Key(object sender, KeyEventArgs e)
        {
            e.Handled = true;
        }

        private void FileDisplay_KeyUp(object sender, KeyEventArgs e)
        {
            if (!(e.KeyData == Keys.Back || e.KeyData == Keys.Enter))
                return;

            if (fileDisplay.SelectedItems.Count > 0)          //vobec daco je aj oznacene
            {
                try
                {
                    if (e.KeyData == Keys.Back || fileDisplay.SelectedItems[0].Text == "..")
                    {
                        if (CurrentDirectory.Parent != null)
                            ShowFolder(CurrentDirectory.Parent);
                        return;
                    }

                    var selItem = fileDisplay.SelectedItems[0] as FilePanelItem;

                    OpenItem(selItem.FileSysInfo);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
        
        private void FileDisplay_Enter(object sender, EventArgs e)      //reguje na klik
        {
            FocusedPanel = this;
        }

        private void FileSelector_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                if (Directory.Exists(FileSelector.Text))
                {
                    ShowFolder(FileSelector.Text);
                    return;
                }

                if (File.Exists(FileSelector.Text))
                {
                    string pr = Path.Combine(Path.GetDirectoryName(FileSelector.Text),
                                             Path.GetFileNameWithoutExtension(FileSelector.Text));

                    var f = new FileInfo(FileSelector.Text);

                    OpenItem(f);
                }
            }
        }

        private void ShowHidden_CheckedChanged(object sender, EventArgs e)
        {
            FillDisplay();
        }
    }
}
