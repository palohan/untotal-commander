﻿namespace DLL
{
    partial class AskDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AskDialog));
            this.YesToAll = new System.Windows.Forms.Button();
            this.NoToAll = new System.Windows.Forms.Button();
            this.ano = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.No = new System.Windows.Forms.Button();
            this.Abor = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // YesToAll
            // 
            this.YesToAll.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.YesToAll.Location = new System.Drawing.Point(12, 101);
            this.YesToAll.Name = "YesToAll";
            this.YesToAll.Size = new System.Drawing.Size(75, 23);
            this.YesToAll.TabIndex = 0;
            this.YesToAll.Text = "YesToAll";
            this.YesToAll.UseVisualStyleBackColor = true;
            // 
            // NoToAll
            // 
            this.NoToAll.DialogResult = System.Windows.Forms.DialogResult.Ignore;
            this.NoToAll.Location = new System.Drawing.Point(93, 101);
            this.NoToAll.Name = "NoToAll";
            this.NoToAll.Size = new System.Drawing.Size(75, 23);
            this.NoToAll.TabIndex = 1;
            this.NoToAll.Text = "NoToAll";
            this.NoToAll.UseVisualStyleBackColor = true;
            // 
            // ano
            // 
            this.ano.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.ano.Location = new System.Drawing.Point(12, 130);
            this.ano.Name = "ano";
            this.ano.Size = new System.Drawing.Size(75, 23);
            this.ano.TabIndex = 2;
            this.ano.Text = "Yes";
            this.ano.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "sumthin";
            // 
            // No
            // 
            this.No.DialogResult = System.Windows.Forms.DialogResult.No;
            this.No.Location = new System.Drawing.Point(93, 130);
            this.No.Name = "No";
            this.No.Size = new System.Drawing.Size(75, 23);
            this.No.TabIndex = 4;
            this.No.Text = "No";
            this.No.UseVisualStyleBackColor = true;
            // 
            // Abor
            // 
            this.Abor.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.Abor.Location = new System.Drawing.Point(174, 113);
            this.Abor.Name = "Abor";
            this.Abor.Size = new System.Drawing.Size(75, 23);
            this.Abor.TabIndex = 5;
            this.Abor.Text = "Abort";
            this.Abor.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(63, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // YES
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(259, 160);
            this.ControlBox = false;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Abor);
            this.Controls.Add(this.No);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ano);
            this.Controls.Add(this.NoToAll);
            this.Controls.Add(this.YesToAll);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "YES";
            this.ShowInTaskbar = false;
            this.Text = "UnTotal Commander";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button YesToAll;
        private System.Windows.Forms.Button NoToAll;
        private System.Windows.Forms.Button ano;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button No;
        private System.Windows.Forms.Button Abor;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}