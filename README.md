### Summary ###
A simple Total Commander WinForms knockoff. You have your 2 panes, can navigate the filesystem, got your action buttons at the bottom. Also can open a .txt file in a separate window. Even resumes at the lastly opened folders! I tested it quite a lot back then - the basic filesystem stuff should work pretty well. 

You are free to use the project in whatever way, but please **DON'T** wreck your system! I hold **NOOO** responsibility whatsoever.

### Background ###
I had my first brush with C# in late 2012 at our Uni, which resulted into a project duly named Homework. After spending my summer with C++, which complexities I could not understand back then, I was amazed how smooth writing C# was (thanks Intellisense). The words *sexiest language* would describe it best. I even thought I would never touch another...

The code looks innocent and naive and my C# skills grew quite bigger since then.

# Have fun! #